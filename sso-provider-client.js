const loginWithSAML = (callback) => {

    let requestDoneCallback = function(loginToken) {

        var loginRequest = {
            saml: true,
            token: loginToken
        };
        Accounts.callLoginMethod({
            methodArguments: [loginRequest],
            userCallback: callback
        });

        loginWindow.close();

    };
    window.SAMLCallback = requestDoneCallback;

    const loginWindow = window.open('/login', 'saml-login-window', '');
};


export default loginWithSAML;