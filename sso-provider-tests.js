// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by sso-provider-server.js.
import { name as packageName } from "meteor/sso-provider";

// Write your tests here!
// Here is an example.
Tinytest.add('sso-provider - example', function (test) {
  test.equal(packageName, "sso-provider");
});
