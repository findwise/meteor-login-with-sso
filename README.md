# Meteor login with SSO
A Meteor plugin for logging in with SSO. Developed for project at Findwise and tested with OneLogin.com and ADFS.


## Quickstart
- Clone this project into `my-project/packages`.
- Add `sso-provider` to the end of `.meteor/packages`.
- On server startup start the sso plugin
```
import {Meteor} from 'meteor/meteor';
import SSOProvider from 'meteor/sso-provider';
Meteor.startup(() => {
    SSOProvider.config({
        serviceProviderCert: Assets.getText('sp-cert.crt'),
        serviceProviderPrivateKey: Assets.getText('sp-key.key'),
        rootUrl: 'https://example.com',
        loginUrl: "https://sts.example.com/adfs/ls",
        identityProviderCertificates: [Assets.getText('idp-certificate.crt')],
        userProfile: function(user) {
            return {
                firstname: user.given_name,
                lastname: user.surname,
                email: user.email
            }
        }
    });
});
```
You can generate serviceProviderCert and serviceProviderPrivateKey yourself: `openssl req -nodes -new -x509 -keyout sp-key.key -out sp-cert.crt -days 1826`. Put these in `my-project/private` and they can be imported with `Assets.getText`.

`userProfile` defines how to map claims from the identity provider to your application. `user` contains the claims.


- Call the login function on the client.

```
import loginWithSAML from 'meteor/sso-provider';
loginWithSAML()
});
```
