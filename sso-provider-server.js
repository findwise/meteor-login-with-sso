import {Picker} from 'meteor/meteorhacks:picker';
import {Meteor} from 'meteor/meteor';
import Fiber from 'fibers';
const bodyParser = Npm.require('body-parser');
const saml2 = Npm.require('saml2-js');
import {Accounts} from 'meteor/accounts-base';




Accounts.registerLoginHandler('saml', (loginRequest) => {
    let user = Meteor.users.findOne({
        'services.sso.requested': loginRequest.token
    });

    if( !user ) {
        return {
            error: 'no user found'
        };
    }

    let userId = user._id;

    let stampedToken = Accounts._generateStampedLoginToken();
    let hashStampedToken = Accounts._hashStampedToken(stampedToken);

    Meteor.users.update(userId,
        { $push: { 'services.resume.loginTokens': hashStampedToken } }
    );


    return {
        userId: userId,
        token: stampedToken.token
    }

});



const SsoProvider = {

    /**
     *
     * @param serviceProviderCert String
     * @param serviceProviderPrivateKey String
     * @param rootUrl String - Where is server hosted. Used to server metadata to identity provider. No trailing slash.
     * @param loginUrl
     * @param logoutUrl
     * @param identityProviderCertificates String
     *
     * @param mapUser function
     * When a response has come from identity provider which user should this map to.
     * The function takes the entire user field of the response as parameter and should return a single value that is the identifier.
     *
     * @param userProfile
     * When a user is first created which fields should be added to their meteor user profile.
     * The function takes the entire user field of the response as parameter and should return and object with profile
     *
     * @returns {*}
     */
    config({serviceProviderCert, serviceProviderPrivateKey, rootUrl, loginUrl, logoutUrl, identityProviderCertificates, mapUser, userProfile}) {




        let sp_options = {
            entity_id: rootUrl + "/metadata.xml",
            assert_endpoint: rootUrl + "/assert",
            certificate: serviceProviderCert,
            private_key: serviceProviderPrivateKey,
        };


        this.sp = new saml2.ServiceProvider(sp_options);



        const idp_options = {
            sso_login_url: loginUrl,
            sso_logout_url: logoutUrl,
            certificates: identityProviderCertificates,
            force_authn: true,
            sign_get_request: false,
            allow_unencrypted_assertion: false
        };

        this.idp = new saml2.IdentityProvider(idp_options);

        this.configRoutes();



        if( mapUser && typeof mapUser === "function") {
            this.mapUserToServiceToken = mapUser;
        }

        if( userProfile && typeof userProfile === "function") {
            this.mapUserToProfile = userProfile;
        }
    },


    configRoutes() {

        let self = this;

        Picker.middleware(bodyParser.json());
        Picker.middleware(bodyParser.urlencoded({extended: false}));



        /*
         * Expose metadata.xml to identity provider
         */
        Picker.route('/metadata.xml', function (params, req, res, next) {
            res.setHeader('content-type', 'application/xml');
            res.end(self.sp.create_metadata());
        });






        /*
         * Expose login url that redirects to the identity providers login page
         * Perhaps this url should be something less obvious. Configurable?
         */
        Picker.route('/login', function (params, req, res, next) {

            self.sp.create_login_request_url(self.idp, {}, function (err, login_url, request_id) {
                res.writeHead(301,
                    {Location: login_url}
                );
                res.end();
            });
        });



        /*

         * Expose assertion url that validates identity providers response
         */

        Picker.route('/assert', function (params, req, res, next) {

            let options = {
                request_body: req.body,
                ignore_signature: true
            };

            self.sp.post_assert(self.idp, options, function (err, saml_response) {


                if (err !== null) {
                    console.error('Something went wrong when asserting SAML response');
                    console.error(err);
                    console.error('----- REQUEST BODY ----');
                    console.error(req.body);

                    return res.end(req.body + "\n" + err);
                }


                // Mongo calls must be inside Fiber to avoid async
                let fiber = Fiber(({saml_response, self}) => {

                    let serviceToken = self.mapUserToServiceToken(saml_response.user);

                    let user = Meteor.users.findOne({
                        'services.sso.token': serviceToken
                    });

                    let userId;
                    if (!user) {

                        userId = Meteor.users.insert({
                            services: {
                                sso: {
                                    token: serviceToken
                                }
                            },

                            profile: self.mapUserToProfile(saml_response.user)
                        });
                    } else {
                        userId = user._id;
                    }


                    let requestId = saml_response.response_header.id;

                    Meteor.users.update({_id: userId}, { $set: { 'services.sso.requested': requestId }});

                    res.end('<html><head><script>window.opener.SAMLCallback("'+requestId+'"); </script></head><body></body></html>');

                });


                fiber.run({saml_response, self});

            });
        });
    },


    mapUserToServiceToken(userAttributes) {
        return userAttributes.name_id;
    },


    mapUserToProfile(userAttributes) {
        return userAttributes.attributes;
        return {
//            firstname: userAttributes.attributes['User.FirstName'][0],
//            lastname: userAttributes.attributes['User.LastName'][0],
//            email: userAttributes.attributes['User.email'][0]
        }
    }


};


export default SsoProvider;